from pymongo.errors import ConnectionFailure
from pymongo import MongoClient

#Build db client and check url

def check_mongo_url():
    try:
        client = MongoClient("mongodb://mongo-0.mongo.db,mongo-1.mongo.db,mongo-2.mongo.db:27017/", connectTimeoutMS=1000, serverSelectionTimeoutMS=1000 )
        db = client.test
        tmp = db.test
        tmp.find_one()
        url="mongodb://mongo-0.mongo.db,mongo-1.mongo.db,mongo-2.mongo.db:27017/"
        return url
    except ConnectionFailure:
        client = MongoClient('mongodb://localhost:27017/')
        db = client.test
        tmp = db.test
        tmp.find_one()
        url="mongodb://localhost:27017/"
        return url


