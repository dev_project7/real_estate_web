import pymysql.cursors

mysql_config = {
    'host':'1.1.1.1',
    'port':3306,
    'user':'root',
    'password':'newpassword',
    'db':'web',
    'charset':'utf8mb4',
    'cursorclass':pymysql.cursors.DictCursor,
}

def check_host():
    try:
        connection = pymysql.connect(**mysql_config, connect_timeout=0.3)
        return mysql_config['host']

    except pymysql.err.OperationalError:

        mysql_config['host'] = '127.0.0.1'
        connection = pymysql.connect(**mysql_config)        
        return mysql_config['host']

def query():
    mysql_config['host'] = check_host()
    # 建立連線
    connection = pymysql.connect(**mysql_config)
    # 執行sql語句
    try:
        with connection.cursor() as cursor:
            
            sql = 'select * from RealTest'  #from this one
            cursor.execute(sql);
            connection.commit()
            result = cursor.fetchall()

    finally:
        connection.close();
    return(result)


if __name__ == '__main__':
    print(type(query()))
    for x in query():
        print(x)
    # print(check_host())