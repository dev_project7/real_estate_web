# REGISTRY="registry.hub.docker.com"
# REPOSITORY="reagantseng/spring"
# tags=`wget -q https://${REGISTRY}v1/repositories/${REPOSITORY}/tags -O -  | sed -e 's/[][]//g' -e 's/"//g' -e 's/ //g' | tr '}' '\n'  | awk -F: '{print $3}'`
# tags=$(echo $tags | rev  | cut -f 1 -d " "  | cut -f 1 -d "v" )
# echo $tags
curl -sSL https://get.docker.com | sudo sh
sudo usermod -aG docker your-user
sudo systemctl sart docker
sudo systemctl enable docker
sudo docker login

#please replace tags
tags=v13.0.2
docker build -t="reagantseng/flask:${tags}" .
docker push  reagantseng/flask:${tags}

sudo docker build -t="reagantseng/flask:latest" .
sudo docker push  reagantseng/flask:latest

kubectl delete configmap web
kubectl create configmap web --from-file=templates/

kubectl delete -f k8s/test/gcp/deployment.yaml
kubectl apply  -f k8s/test/gcp/deployment.yaml
kubectl delete -f k8s/test/gcp/service.yaml
kubectl apply  -f k8s/test/gcp/service.yaml

# kubectl delete -f k8s/test/gcp/flask.ingress.yaml
# kubectl apply  -f k8s/test/gcp/flask.ingress.yaml
flask=$(kubectl get pods | grep -E "^flask" | head -n 1 | cut -d " " -f 1 )
kubectl logs $flask