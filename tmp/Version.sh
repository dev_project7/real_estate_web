# for dockerhub use
#REGISTRY="registry.hub.docker.com"
# REPOSITORY="reagantseng/flask"
# tags=`wget -q https://${REGISTRY}v1/repositories/${REPOSITORY}/tags -O -  | sed -e 's/[][]//g' -e 's/"//g' -e 's/ //g' | tr '}' '\n'  | awk -F: '{print $3}'`
# tags=$(echo $tags | rev  | cut -f 1 -d " "  | cut -f 1 -d "v" )
# echo $tags


# for Google use
tags=$(gcloud container images list-tags gcr.io/affable-elf-255205/flask | tail -n 1 |uniq | cut -f 3 -d " " | rev | cut -f 1 -d "," | cut -f 1 -d "v")
echo $tags

if [[ -n ${tags} ]]; then
    tags=$((${tags}+1))
else
    tags="1"
fi
tags=v$tags
echo $tags