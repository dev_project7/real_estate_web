# Tutorial
https://cert-manager.io/v1.2-docs/tutorials/acme/ingress/
# 我的筆記
#    https://cert-manager.io/v1.2-docs/installation/kubernetes/   
```
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx   
#helm install <ingress-name> ingress-nginx/ingress-nginx  # ingress-name要和ingress-tls-final.yaml匹配  
helm install quickstart ingress-nginx/ingress-nginx  
```
#    Try it : helm install try ingress-nginx/ingress-nginx
```
kubectl get svc    
#這裡的name也要修改，Ingress會指定backend是哪個service  
wget https://netlify.cert-manager.io/docs/tutorials/acme/example/deployment.yaml    
wget https://netlify.cert-manager.io/docs/tutorials/acme/example/service.yaml    
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```
#   build cert-manager  
```
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml
# create clusterrolebinding.rbac  
kubectl get pods --namespace cert-manager
kubectl create clusterrolebinding cluster-admin-binding \
    --clusterrole=cluster-admin \
    --user=$(gcloud config get-value core/account)
kubectl get pods --namespace cert-manager

```


#   ingress-tls-final 
```
wget https://cert-manager.io/docs/tutorials/acme/example/ingress-tls-final.yaml    
#hosts填A record  
vim ingress-tls-final.yaml
kubectl create -f ingress-tls-final.yaml
kubectl describe certificate quickstart-example-tls
```

#   issuer
```
wget https://cert-manager.io/docs/tutorials/acme/example/production-issuer.yaml  
vim production-issuer.yaml  
kubectl create -f production-issuer.yaml  
kubectl get issuer  
# describe order status  
kubectl describe order $your_order_name  
```
