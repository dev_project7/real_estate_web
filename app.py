from flask import Flask, request, render_template, redirect, url_for, flash
#import mysql.connector as mariadb  #discard
import pandas as pd
from pymongo import MongoClient
from operate_db.mongo import check_mongo_url


#active app
app = Flask(__name__)

#def init():
    #!/usr/bin/python

# @app.route('/login', methods=['GET', 'POST'])
# def login():
#     if request.method == 'POST':
#         if login_check(request.form['username'], request.form['password']):
#             flash('Login !')
#             return redirect(url_for('t', username=request.form.get('username')))
#     return render_template('login4.html')

# def login_check(username, password):
#     """登入帳號密碼檢核"""
#     if username == 'HelloKitty' and password == '1234':
#         return True
#     else:
#         return False

# @app.route('/t/<username>')
# def t(username):
#     return render_template('managerConsol.html', username=username)

# @app.route('/arch')
# def arch():
#     return render_template('architectureIntroduct.html')

# @app.route('/manger')
# def manger():
#     return render_template('login4.html')

# @app.route('/thanks')
# def thanks():
#     return render_template('thanks.html')

# @app.route('/author')
# def author():
#     return render_template('author.html')

@app.route('/')
def index():

    url = check_mongo_url()
    client = MongoClient(url)
    db = client.web
    operation = db.web
    ret = operation.find_one()

    #delete mongodb id column
    del ret["_id"]

    if ret == None:
	    return render_template('404.html')
    else:
        return render_template('index.html', foo = ret)
    #foo2=tmpcsv


if __name__ == '__main__':
    #tmpcsv = init()
    app.debug = True
    app.secret_key = '5f352379324c22463451387a0aec5d2f'
    app.run(host='0.0.0.0',port=5000)


