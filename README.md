<!-- ![alt text](./static/p1.jpg "Title")
![alt text](./static/p3.jpg "Title")
![alt text](./static/p2.jpg "Title")
![alt text](./static/p4.jpg "Title") -->

## v13 (目前版本)
* 將html移到configmap以避免每次修改前端必須重新docker build, 使用Letsencrypt-ingress-k8s加密
* (test) 重建CI for Kubernetes, 使用job或sidecar確認DB狀況  
## 整體分成3個部分
- 平台管理     : 包括 GKE, Gitlab CI
- 基本服務建置  : 包括 : MongoDB, MySQL, Letsencrypt-Ingress-Nginx 
- 後端代碼     : 包括 : Python & Golang 

## v12 
* update package pymongo 4.0 anf change package from mysql.connector to "pymysql.cursors"  
* 增加2020,2021資料, 將data file從.py檔中抽離,  優化Dockerfile的build與push時間  

## v11
* 完成網站主要程式碼並上版到k8s  
## other
* 修正CI/CD，從呼叫gcp api改成use-context  
* 將APP的機敏參數分離給secret.yaml configmap.yaml  
* 修改RBAC與Service Account
